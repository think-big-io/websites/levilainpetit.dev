---
title: Root/
layout: home
permalink: /
---
<style>
/* On screens that are 600px or less, set the background color to olive */
@media screen and (max-width: 600px) {
  #minwidth600{
      display: none;
  }
  #maxwidth600{
      display: block;
  }
}

@media screen and (min-width: 600px) {
  #minwidth600{
      display: block;
  }
  #maxwidth600{
      display: none;
  }
}
</style>
<pre id="maxwidth600">
      __              _ _       _       
     / /  ___  __   _(_) | __ _(_)_ __  
    / /  / _ \ \ \ / / | |/ _` | | '_ \ 
   / /__|  __/  \ V /| | | (_| | | | | |
   \____/\___|   \_/ |_|_|\__,_|_|_| |_|
                                        
               _   _ _        ___           
    _ __   ___| |_(_) |_     /   \_____   __
   | '_ \ / _ \ __| | __|   / /\ / _ \ \ / /
   | |_) |  __/ |_| | |_   / /_//  __/\ V / 
   | .__/ \___|\__|_|\__| /___,' \___| \_/  
   |_|                                          
         __
     ___( o)>
     \ <_. )
     `---'   rd
</pre>
<pre id="minwidth600">
    ██╗     ███████╗    ██╗   ██╗██╗██╗      █████╗ ██╗███╗   ██╗
    ██║     ██╔════╝    ██║   ██║██║██║     ██╔══██╗██║████╗  ██║
    ██║     █████╗      ██║   ██║██║██║     ███████║██║██╔██╗ ██║
    ██║     ██╔══╝      ╚██╗ ██╔╝██║██║     ██╔══██║██║██║╚██╗██║
    ███████╗███████╗     ╚████╔╝ ██║███████╗██║  ██║██║██║ ╚████║
    ╚══════╝╚══════╝      ╚═══╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
                                                                
    ██████╗ ███████╗████████╗██╗████████╗    ██████╗ ███████╗██╗   ██╗
    ██╔══██╗██╔════╝╚══██╔══╝██║╚══██╔══╝    ██╔══██╗██╔════╝██║   ██║
    ██████╔╝█████╗     ██║   ██║   ██║       ██║  ██║█████╗  ██║   ██║
    ██╔═══╝ ██╔══╝     ██║   ██║   ██║       ██║  ██║██╔══╝  ╚██╗ ██╔╝
    ██║     ███████╗   ██║   ██║   ██║       ██████╔╝███████╗ ╚████╔╝ 
    ╚═╝     ╚══════╝   ╚═╝   ╚═╝   ╚═╝       ╚═════╝ ╚══════╝  ╚═══╝  
        __
    ___( o)>
    \ <_. )
    `---'   rd
</pre>

