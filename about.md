---
layout: page
title: About/
permalink: /about/
---
# Pourquoi ?

Parcequ'un développeur, ca râle bien souvent car : 
  * le stagiaire à supprimer une table en production, 
  * le code existant est moche et incompréhensible,
  * le code à changé mais pas les tests et cela casse le pipeline
  * et bien d'autres situations encore ...

Et qu'en plus, aux yeux des autres utilisateurs, nous ne sommes que des étranges créatures avec un boulot dont on ne comprends pas forcement le but (sinon pourquoi on nous demanderait de réparer le graveur CD qui ne marche pas?)
<br/>
Il faut bien de temps en temps également partager nos retours d'expériences "compliquées" pour ne pas dire agaçant (avec des fois l'envie de couper les mains à certains)
Si on ne partage que le super cas qui fonctionne, ou la super organisation qui tourne toute seule, nous avons le sentiment qu'en tant que râleur, nous sommes tout seul et qu'on emmerde le monde plutôt qu'autre chose.

<br/>
Détrompe toi, tu n'es pas seul(e) dans ce cas et si tu veux, tu peux même partager avec nous les situations que tu as rencontrées. Si en plus, tu as trouvé une solution, pourquoi pas la partager avec tout le monde.


# Anonyme?
Comme disais Rubem Fonseca: 
> "Compte-rendu de circonstances où toute ressemblance n'est pas pure coïncidence."

Mais souhaitant garder de bonnes relations avec les communautés IT / Devops et autres, nous ne citerons aucun nom. 
Nous les remplaceront pas des noms de code fictifs basé sur un générateur de code pour éviter tout lien avec le monde réel.

# Qui sommes nous ?
Des développeurs, des devops, des "couteaux-suisses" pour ne pas dire "homme/femme à tout faire", des experts de tout et de rien, bref des gens comme vous et moi.

# Contribuer
Si déjà vous voulez contribuer, vous n'êtes pas obligé aprés tout. 
Le plus simple est de passer par le formulaire de [**contact**](/contact) pour nous contacter.
Ultérieurement, nous mettrons en place un Slack ou un Discord pour pouvoir échanger.

# FAQ
## Pourquoi on ne peut pas commenter les articles ?
Par soucis de temps, nous préférons prendre du temps à échanger en direct avec les lecteurs par email ou par Slack, qu'en perdre à supprimer le SPAM Bot qui envoie des commentaires avec des liens vers des sites peu recommandable, ou les trolls dont la mission est de nous faire perdre notre temps avec des commentaires sans aucun sens.
