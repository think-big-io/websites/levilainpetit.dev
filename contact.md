---
layout: page
title: Contact/
permalink: /contact/
---

# Nous contacter ?
Pour dire quoi? Que ce soit pour crier au scandale, ou nous féliciter, vous pouvez nous contacter via le formulaire ci dessous.
Nous vous répondrons quand le dev qui a fait tomber le serveur SMTP aura remit de l'ordre dans le réfactoring sans test qu'il a fait, et quand le sprint de 3 mois sera terminé, alors nous pourrons plannifier au prochain CAB ( «Change Advisory Board» ) la récupération des emails et implémenter le bouton "Répondre" sur notre outil fait maison pour lire et envoyer des emails.
<br/>
Non, trève de plaisanterie, on vous répondra ASAP ( «As Soon As Possible» ) si vous nous contacté votre email, sinon, la magie ne pourra pas opérer et vous n'aurez pas de réponse de notre part ;-)
<br/>

<form name="contact" method="POST" data-netlify="true" data-netlify-recaptcha="true" enctype="application/x-www-form-urlencoded">
  <input type="hidden" name="form-name" value="contact" />
  Nom: 
  <input style="border:1px dotted white; margin:5px; padding:5px;" type="text" id="name" name="name" placeholder="Saisissez votre nom #pseudo #ceQueVousVoulez ici" autocomplete="off">
  Email:
  <input style="border:1px dotted white; margin:5px; padding:5px;"  type="text" id="email" name="email" placeholder="Saisissez ici votre email si vous voulez une réponse de notre part" autocomplete="off">
  Message: 
  <textarea style="border:1px dotted white; margin:5px; padding:5px;"  rows="5" id="message" name="message" placeholder="Et là, votre message sous forme de prose, de lettre ouverte mais s'il vous plait évitez le SMS mode, merci" autocomplete="off"></textarea>
  
  <div data-netlify-recaptcha="true"></div>

  <input style="border:1px dashed white; margin:5px; padding:5px;" type="submit" value="[ Envoyer ]">
  <br/>
  <br/>
  <span style="font-size:0.7em;">Conformément à la loi Informatique et Libertés du 6 janvier 1978, vous disposez d'un droit d'accès et de rectification aux données personnelles vous concernant. Seule notre société est destinataire des informations que vous lui communiquez.</span>
</form>

<style> .g-recaptcha { margin-left: 5px; } </style>