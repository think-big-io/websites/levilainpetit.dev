# Master
[![pipeline status](https://gitlab.com/think-big-io/websites/levilainpetit.dev/badges/master/pipeline.svg)](https://gitlab.com/think-big-io/websites/levilainpetit.dev/-/commits/master)
[![coverage report](https://gitlab.com/think-big-io/websites/levilainpetit.dev/badges/master/coverage.svg)](https://gitlab.com/think-big-io/websites/levilainpetit.dev/-/commits/master)
[![Gitpod Ready-to-Code-Master](https://img.shields.io/badge/Gitpod-Ready--to--Code--Master-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/think-big-io/websites/levilainpetit.dev) 
[![Netlify Master Status](https://api.netlify.com/api/v1/badges/c845cb95-7374-4cac-8f02-55a52ec764f4/deploy-status)](https://app.netlify.com/sites/preview-levilainpetit-dev/deploys)

# Development
[![pipeline status](https://gitlab.com/think-big-io/websites/levilainpetit.dev/badges/develop/pipeline.svg)](https://gitlab.com/think-big-io/websites/levilainpetit.dev/-/commits/develop)
[![coverage report](https://gitlab.com/think-big-io/websites/levilainpetit.dev/badges/develop/coverage.svg)](https://gitlab.com/think-big-io/websites/levilainpetit.dev/-/commits/develop)
[![Gitpod Ready-to-Code-Development](https://img.shields.io/badge/Gitpod-Ready--to--Code--Development-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/think-big-io/websites/levilainpetit.dev/-/tree/develop)
[![Netlify Develop Status](https://api.netlify.com/api/v1/badges/c845cb95-7374-4cac-8f02-55a52ec764f4/deploy-status)](https://app.netlify.com/sites/develop--preview-levilainpetit-dev/deploys)