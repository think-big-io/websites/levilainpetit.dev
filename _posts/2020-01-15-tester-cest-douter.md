---
layout: article
title:  "Tester c'est douter"
date:   2020-01-15 12:30:00 +0000
categories: test
---

Combien de fois j'ai entendu :
>Tester c’est douter

Ou encore
> Ca sert à rien les tests, c'est chiant à coder et à maintenir

Certains pensent que ceux qui doutent, ce sont les débutants. Ceux qui ne sont pas sûrs de ce qu’ils font. 
Eux ils doivent tester, mais les developpeurs plus expérimenté n'en ont plus besoin. Ils ont dépassés ce stade.

<br/>

Et si comme moi, vous êtes un développeur avec une dizaine d'années d'expériences et nous décidions également d'arrêter de douter. 
Aprés tout c’est inutile, nous valons bien mieux que ça. 

Ne testons plus, car ça fait perdre du temps. Nous allons donc pouvoir coder plus vite et votre hiérarchie sera plus satisfaite de vous.
