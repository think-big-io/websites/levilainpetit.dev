---
layout: article
title:  "Le commit politique"
date:   2019-10-15 12:30:00 +0000
categories: developpement
---

## C'est quoi donc "le commit politique" ?
Le principe est trés simple, on vous demande de faire ce type de "commit" afin de passer une user story à terminée avant la fin du Sprint même si celle ci n'est pas terminée, voir même pas commencée.
Tout cela dans le but de livrer ce que les chefs avaient prévues en CAB auparavant dans le version sur laquel vous travaillez et avant la date butoire de livraison.

### Un exemple sera peut être plus clair. 
Pour la version 3R7cqZ6R, il a été décidé que la fonctionnalité US-cfq49Bmt devait être livré. Mais on arrive au terme du sprint, avant la livraison et la fonctionnalité n'a même pas encore été développée.
Que faire ? Dire à la MOA que l'US ne sera pas livrée à temps. S'excuser auprés des clients finaux. 
Devoir remplir une tonne de paperasse pour avoir une dérogation pour livrer dans une version intermédiaire qui n'a pas été programmé et qui necessiterait de réunir le CAB et les Devops et les managers et les CP ( en bref, le genre de celule de crise où la terre entière est en copie) pour avoir une date prévisionnelle si le CAB accepte la livraison intérmédiaire.

### Ou ...
On ne fait qu'implémenter la partie visible de l'iceberg, c'est à dire la partie IHM, mais on n'implémente pas le code métier derrière, le tout sans le dire à personne (c'est la surprise).
On livre en l'état et on dira au moment de la recette qu'il doit s'agir d'un bug non qualifié et qu'il faudra corriger cela dans une version corrective ultérieurement.
Au moins, pas de paperasse, pas de réunion. Juste un développeur qui a des remords d'avoir envoyé un code non fonctionnel sous son nom, qui passera pour un nul dans la mesure où son developpement ne fonctionne pas et qu'apparement, il ne l'a même pas testé. 
Mais aprés tout, un développeur, qui ce soucis de lui, il est là pour "cracher des lignes de code". 
Pour éviter de trop l'accabler, le chef de projet dira aux clients qu'il faut qu'il organise une réunion interne avec l'équipe pour éviter que ce genre de situation ne se répète.
Et en plus, il passera pour le sauveur.

Voilà, ce qu'est un **"commit politique".**

## Comment quand on est un dev conscient qui souhaite livrer de la fonctionnalité fonctionnelle avec le moins de bug possible (car il y a toujours un bug qu'on a pas vu ou un cas qui n'a pas été qualifié) ?
Ben, en faites, malgré la demande, (ce que j'ai tenté de faire mais sans succés) on tente de faire du quick'n dirty et de livrer une fonctionnalité "qui fonctionne" quand même. 
On fait tout sauter, les phases de rédactions des tests unitaires, le réfactoring, la qualité de code...
On code au mieux, au plus vite et on ne fais que les tests de base à la main pour s'assurer que le code ajouté :
- compile bien
- ne casse pas l'application lors l'installation
- réponds au moins au cas de base (le plus fréquent)
- n'entraine pas de régression fonctionnelle

# Mes petits TIPS
* Evitez de rajouté du code dans une classe déjà jugée comme inmaintenable
* Préférez isoler le code dans une classe à part
* Ajoutez des commentaires comportant le numéro de la user story qui fait l'objet d'un "commit politique" afin de retrouver plus facilement le code à nettoyer
* Compilez le projet
* Lancez au moins une fois votre application pour tester qu'elle fonctionne toujours
* Testez au moins le cas de plus fréquent
* Témpérez votre conscience professionnelle si vous souhaitez garder votre job ou changez de client, ou de job
* Surtout, faite bien comprendre à votre N+1 que cette pratique ne doit pas se reproduire à l'avenir et que vous n'êtes pas d'accord car cela dégrade l'image du produit, et la confiance que le client à placé dans l'équipe de développement

Si vous avez d'autres TIPS, nous sommes preneur de vos retours d'expériences.
Que la force soit avec vous et je vous encourage à toujours faire de votre mieux.